#
# Copyright (c) Tomasz Stochmal 2020
#
# License: https://opensource.org/licenses/NPOSL-3.0
#
from __future__ import print_function

import os
import time
import socket

from pprint import pprint

#
# defaults
#
PORT = 49002
FILENAME = 'flight.kml'
FILENAME_LINK = 'flight_link.kml'
VERSION = 'aerofly-fs2-google-earth-pro v1.1.0'

#
# can adjust to tweak
#
MAX_POS = 1         # more and Google Earth is becoming slow
FLUSH_SEC = 1
SPEED_DELTA = 25
SKIP_POS = 5        # 0.2 updated interval = 5 updates / seconds for XGPSAerofly message

#
# do not change below
#
POS = []
LAST_FLUSH = time.time()
LAST_LEN = 0
LAST_DATA = None
GPS_COUNT = -1

def main():

    global LAST_FLUSH
    global LAST_LEN
    global POS

    print(VERSION)

    hostname = socket.gethostname()
    ip = socket.gethostbyname(hostname)

    print('Socket listen on:',hostname,ip+':'+str(PORT))

    sock=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((ip,PORT))
    sock.settimeout(FLUSH_SEC) # seconds
    while True:
        try:
            dataMessage = sock.recvfrom(1024)

            data = dataMessage[0]
            savePosition(data)

        except socket.timeout:
            pass

        if time.time() - LAST_FLUSH >= FLUSH_SEC:
            LAST_FLUSH = time.time()

            if LAST_LEN != len(POS):
                LAST_LEN = len(POS)
                writeXML()


def savePosition(message):

    global POS
    global LAST_DATA
    global GPS_COUNT

    mess = message.decode('utf-8')

    mess = mess.replace('(','')
    mess = mess.replace(')','')
    mess = mess.replace("'","")

    if mess.startswith('XGPSAerofly FS 2'):

        GPS_COUNT+=1
        if GPS_COUNT % SKIP_POS == 0:

            if LAST_DATA != mess:
                LAST_DATA = mess

                print(mess,'\r',end="")

                buf = mess.split(',')
                proto,log,lat,alt,hed,spe = buf[0],buf[1],buf[2],buf[3],buf[4],buf[5]

                spe = str(int(float(spe) * 3.6)) # convert m/s to km/h

                POS.append((log,lat,alt,hed,spe))




XML = '''<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
<Document>
    <name>Aerofly FS2 flight</name>

    <Style id="flighttrack">
        <IconStyle>
            <scale>1.0</scale>
            <color>ff00ffff</color>
            <Icon>
                <href>http://maps.google.com/mapfiles/kml/shapes/airports.png</href>
            </Icon>
        </IconStyle>
    </Style>

    <Folder>
        <name>Aerofly FS2 track</name>
        <open>1</open>
        %s
    </Folder>

</Document>
</kml>'''

POINT = '''
<LookAt>
    <longitude>%(log)s</longitude>
    <latitude>%(lat)s</latitude>
    <altitude>%(alt)s</altitude>
    <heading>%(hed)s</heading>
    <tilt>0</tilt>
    <range>5000</range>
    <gx:altitudeMode>relativeToSeaFloor</gx:altitudeMode>
</LookAt> 

<Placemark>
    <styleUrl>#flighttrack</styleUrl>
    <Point>
        <extrude>1</extrude>
        <altitudeMode>absolute</altitudeMode>
        <coordinates>%(log)s,%(lat)s,%(alt)s</coordinates>
    </Point>
    <Style>
        <IconStyle>
            <heading>%(hed)s</heading>
        </IconStyle>
    </Style>
    <name>%(spe)s</name>
</Placemark>'''

LINK = '''<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
<NetworkLink>
    <name>Aerofly FS2 link</name>
    <open>1</open>

    <flyToView>1</flyToView>
    <Link>
        <href>%(filename)s</href>
        <refreshMode>onInterval</refreshMode>
        <refreshInterval>1</refreshInterval>
    </Link>
</NetworkLink>
</kml>
'''


def writeXML():

    global POS

    last_speed = 0
    last_pos = None
    points=''
    for pos in POS[-MAX_POS:]:
        speed = int(pos[4])
        if abs(speed - last_speed) >= SPEED_DELTA:
            last_speed = speed
        else:
            pos = list(pos)
            pos[4]=''
            pos = tuple(pos)

        points+=(POINT % {'log':pos[0],'lat':pos[1],'alt':pos[2],'hed':pos[3],'spe':pos[4]})
        last_pos = pos

    xml = XML % points

    print('\nsaving',len(POS[-MAX_POS:]),'gps locations to',FILENAME)

    file=open(FILENAME,'w')
    file.write(xml)
    file.close()

    if not os.path.exists(FILENAME_LINK):
        xml = LINK % {'filename':os.path.abspath(FILENAME),'log':last_pos[0],'lat':last_pos[1],'alt':last_pos[2],'hed':last_pos[3]}

        file=open(FILENAME_LINK,'w')
        file.write(xml)
        file.close()



if __name__=='__main__':

#   savePosition("('XGPSAerofly FS 2,-106.9192,39.6442,3370.8,288.8,68.2', ('192.168.1.30', 54937))")

    main()

