# aerofly-fs2

Track position of your fligth in Google Earth Pro in real time

to execute this app you need to download latest [Python 3](https://www.python.org/downloads/)

on command line simply run \
`python TrackMap.py` \
or \
`python MovingMap.py`


start flying so data is recieved and 'flight.kml' is generated in app folder
    
in Google Earth Pro ([desktop version](https://www.google.com/earth/versions/#earth-pro)) you need to add 'Network Link' to generated file
go to 'Add/Network Link' and use 'Browse...' button to open 'flight.kml' for track map

or
go to 'File/Open...' and select 'flight_link.kml' for moving map

[Google Earth Network Links Explained](http://www.gelib.com/google-earth-network-links.htm)

Enable **broadcast** option in the Aerofly FS2 and use default port 49002

[Track map video](https://youtu.be/7QlAW5ojRcU) \
[Moving map video](https://youtu.be/wleuPXOq9BU)

More info at [forum](https://www.aerofly.com/community/forum/index.php?thread/16269-track-your-flight-in-google-earth-real-time/)

